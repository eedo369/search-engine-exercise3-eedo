It is Tuesday, this happened on Saturday evening and I forgot to post until now. It was a little before dinner, when our store sees the most traffic.

I work at a retail store in a busy mall. We are very "hip," meaning the store plays loud pop music, is very bright, and carries a huge amount of vibrant colors, sparkles, and product all smashed together in very close proximity. All this to say: it can be a little overstimulating on a Saturday night when we are full of customers.

I have been working here for three weeks or so and Manager's main critique so far is being a little louder and more assertive about greeting customers and striking up conversation. I'm an introvert, so it's unnatural but I can turn it on when I need to. Since the store is loud and grabs people's attention when they walk in, I have a hard time competing with the ambience.

Anyway, it's been a long day already, I'm tired, and a bit irritated that Manager keeps insisting I engage with every single customer, even when I'm at the register, far away, or they blatantly ignore me because they don't want to be bothered when they shop (which I totally understand and I try to make it polite and natural as much as I can).

So a man and his teenage daughter walk in, face a table so that they are at a right angle to me. I call out "Hi, how are y'all?" but they obviously don't hear me and they can't see me have from where they are standing, so, a little irritated because Manager saw this failed contact happen, I stride over within three feet and confidently ask in a strong, self-assured manner, "How are you finding things tonight?" The girl startles and fucking FAINTS, crumpling to the floor against her dad's leg. Dad catches her, stabilizes, and then looks at me confused. "What?!" he asks, assuming I was coming to alert them of an emergency or something.

Cut to Manager and I trying our damndest to apologize, get the girl some water once she comes to (thankfully in like 20 seconds), and play it off so she won't be embarrassed, which she obviously is, with Manager trying to make sure they don't sue us for my being too aggressive in my greeting.

What a fucking day.