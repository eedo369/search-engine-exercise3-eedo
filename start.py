# ================================ imports ================================
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import string
import collections
import math
from pandas import read_csv
# ================================ General Variables ================================

# Implemented according to
# https://janav.wordpress.com/2013/10/27/tf-idf-and-cosine-similarity/

FILES_PATH = './data/'
LANGUAGE = 'english'

normalize_values = {
    '0': 'zero',
    '1': 'one',
    '2': 'two',
    '3': 'three',
    '4': 'four',
    '5': 'five',
    '6': 'six',
    '7': 'seven',
    '8': 'eight',
    '9': 'nine',
    '10': 'ten',
    '$': 'dollar',
    '%': 'percent',
    '&': '',
    '#': 'hashtag'
}


# ================================ Functions ================================

def my_tokenization_function(document_text):
    document_text = document_text.encode('utf-8')
    document_text = document_text.decode('unicode-escape')
    stop_words = set(stopwords.words('english') + list(string.punctuation))
    ps = PorterStemmer()

    # Tokenize
    terms = word_tokenize(document_text)

    # Normalize
    terms = [term if term not in normalize_values else normalize_values[term] for term in terms]

    # Remove stops
    terms = [term for term in terms if term not in stop_words]

    # Stem
    terms = [ps.stem(term.lower()) for term in terms]

    return terms


class IREngine(object):

    def __init__(self):
        self.documents = {}
        self.inverted_index = {}

    # Build inverted index
    def index(self, idx, document_text, tokenization_function):
        # In addition to building the index, we count and store
        # the number of terms per document in
        # 'self.documents[idx]['words_count']'
        # and the number of times a term has appeared in a document in
        # 'self.inverted_index[term][idx]['term_count']'
        #
        # These two properties will be uses to calculate tf

        self.documents[idx] = {}
        self.documents[idx]['document_text'] = document_text

        terms = tokenization_function(document_text)
        self.documents[idx]['terms'] = [None]*len(terms)
        self.documents[idx]['terms'] = terms

        self.documents[idx]['words_count'] = len(terms)

        for term in terms:
            if term in self.inverted_index:
                term_count_in_document = 1
                if idx in self.inverted_index[term]:
                    term_count_in_document = 1 + int(self.inverted_index[term][idx]['term_count'])
                    self.inverted_index[term][idx]['term_count'] = term_count_in_document
                else:
                    self.inverted_index[term][idx] = {}
                    self.inverted_index[term][idx]['term_count'] = 1

            else:
                self.inverted_index[term] = {}
                self.inverted_index[term][idx] = {}
                self.inverted_index[term][idx]['term_count'] = 1

    # Sort inverted index
    def index_sort(self):
        self.inverted_index = collections.OrderedDict(sorted(self.inverted_index.items()))

    # Calculate tf and idf per term
    def calc_term_tfidf(self):
        # Formula for calculation of tf per document is:
        # term[tf] = Number of times the term appears in the document / Total number of terms in the document
        #
        # Formula for calculation of idf per document is:
        # term[idf] = log(Total number of documents / 1 + Number of documents with term in it)
        # We add 1 to the denominator in order to avoid
        # dividing by zero when the term is not in the corpus

        for term in self.inverted_index:
            #calc tf
            for idx in self.inverted_index[term]:
                self.inverted_index[term][idx]['tf'] = self.inverted_index[term][idx]['term_count'] / self.documents[idx]['words_count']

            #calc idf
            self.inverted_index[term]['idf'] = math.log(len(self.documents) / (1 + len(self.inverted_index[term])))

    def calc_query_cos_sim(self, terms):
        # Formula for cosine_similarity per document:
        # Cosine Similarity(Query,Document1) = Dot product(Query, Document1) / ||Query|| * ||Document1||
        # where Query is a list of all the terms in a user query

        # In our script:
        # Dot product(Query, Document1) is:
        # 'document_temp[idx]['document_sum']'
        #
        # ||Query|| is:
        # '\query_sum'
        #
        # ||Document1|| is:
        # 'document_temp[idx]['product']'
        #
        # Cosine Similarity(Query,Document1) is:
        # cosine_documents[idx]

        cosine_documents = {}
        document_temp = {}
        query = {}

        # calc tf*idf of query to self
        # O(m) <= number of terms
        query_sum = 0
        for term in terms:
            query[term] = {}
            if term in self.inverted_index:
                query[term]['tfidf'] = (1 / len(terms)) * self.inverted_index[term]['idf']
            else:
                query[term]['tfidf'] = float(0)
            # ||Query||
            query_sum += (query[term]['tfidf'] * query[term]['tfidf'])

        # ||Query||
        query_sum = math.sqrt(query_sum)

        # O(n) <= number of documents
        for idx in self.documents:
            document_temp[idx] = {}
            document_temp[idx]['document_sum'] = float(0)
            document_temp[idx]['product'] = float(0)
            for term in query:
                document_temp[idx][term] = {}
                # calc tf*idf of query to all documents
                if term in self.inverted_index and idx in self.inverted_index[term]:
                    document_temp[idx][term]['tfidf'] = self.inverted_index[term][idx]['tf'] * self.inverted_index[term]['idf']
                else:
                    document_temp[idx][term]['tfidf'] = float(0)

                # ||Document1||
                document_temp[idx]['document_sum'] += (document_temp[idx][term]['tfidf'] * document_temp[idx][term]['tfidf'])

                # Dot product(Query, Document1)
                document_temp[idx]['product'] += (query[term]['tfidf'] * document_temp[idx][term]['tfidf'])

            # ||Document1||
            document_temp[idx]['document_sum'] = math.sqrt(document_temp[idx]['document_sum'])

            # Cosine Similarity
            sum_multiply = (query_sum * document_temp[idx]['document_sum'])
            if(sum_multiply != 0):
                cos_sin = document_temp[idx]['product'] / sum_multiply
            else:
                cos_sin = float(0)
            cosine_documents[idx] = cos_sin

        cosine_documents = sorted(cosine_documents.items(), key=lambda x: x[1], reverse=True)
        return cosine_documents

    def get_document_by_index(self, idx):
        return self.documents[idx]


def get_search_query():
    '''get user input search string'''

    return input("Please enter search query: ")


def get_search_type():
    ''' get user search type'''

    ans_valid = False
    while (not ans_valid):
        t = input(
'''please select the type of search you would like to run:
0. print inverted index
1. search with ranking
2. find similar documents
3. print document
q. exit
 enter selection: '''
        )

        if 'q' in t:
            exit()
        if t not in ('0', '1', '2', '3'):
            print("ERROR: invalid value in answer please enter selected number [ 0, 1, 2, 3, 4 ]")
        else:
            return t


def main():
    engine = IREngine()
    # files = [FILES_PATH + '/1.txt']

    corpus = read_csv(FILES_PATH + 'tweets.csv')
    tweets = corpus['text']

    for index in range(1,21):
        with open(FILES_PATH + str(index) + '.txt', 'r', encoding="utf-8") as f:
            text = f.read()
            engine.index(index, text, my_tokenization_function)

    for i, tweet in enumerate(tweets):
        engine.index(i, tweet, my_tokenization_function)

    # Sort the index by term
    engine.index_sort()

    # Calculate tf*idf per term
    engine.calc_term_tfidf()

    # user input loop
    cont = True
    while (cont):

        # select search type
        q_type = get_search_type()

        if (int(q_type) == 0):
            print(engine.inverted_index)
        elif (int(q_type) == 1):
            #  get search query from user
            query = get_search_query()

            #  get matching files index
            terms = my_tokenization_function(query)

            if len(terms) > 0:
                res = engine.calc_query_cos_sim(terms)
                #print(res)
                print('results:  {0}'.format(query))
                print('{}'.format(
                    ["document: {0}, cosine: {1}".format(id[0], id[1]) for id in res]))
                #print('results:  {0}, {}'.format(
                #    query, ["file: {1}".format(id) for id in res[query]]))
            else:
                print("Error: invalid query params, some tokens might have been cut off as stop words")

        elif (int(q_type) == 2):
            idx = input("Please enter a document number: ")
            query = engine.get_document_by_index(int(idx))
            res = engine.calc_query_cos_sim(query['terms'])
            print('similar documents for document {0}:  '.format(idx))
            print('{}'.format(
                ["file: {0}, cosine: {1}".format(id[0], id[1]) for id in res]))
        elif (int(q_type) == 3):
            idx = input("Please enter a document number: ")
            print(engine.get_document_by_index(int(idx)))

    return


if __name__ == '__main__':
    main()